import QtQuick 1.1
import com.nokia.meego 1.0
import QtMultimediaKit 1.1

PageStackWindow {
    id: appWindow

    initialPage: mainPage

    property bool stopClicked: false

    function selectStation(name, url)
    {
        stationName.text = "כרגע משמיע: " + name;
        audioPlayback.source = url;
        audioPlayback.play();
    }

    Page {
        id: mainPage

        Text {
            id: stationName
            font.pointSize: appWindow.width/text.length
            anchors.centerIn: mainPage
            anchors.horizontalCenterOffset: -10
            width: inPortrait? appWindow.height-20 : appWindow.width-20
            text: "טרם נבחרה תחנה, אנא בחרו מהרשימה"
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: "AlignHCenter"
            wrapMode: "WrapAtWordBoundaryOrAnywhere"
        }

        Text {
            id: warning
            anchors.bottom: mainPage.bottom
            anchors.bottomMargin: 20
            anchors.left: inPortrait? mainPage.top : mainPage.left
            anchors.right: inPortrait? mainPage.bottom : mainPage.right
            anchors.rightMargin: 10
            font.pointSize: 18
            text: "*** מאחר ותוכנה זו משמיעה את תחנות הרדיו על גבי חיבור אינטרנט,"+
" שימוש בה בחיבור אינטרנט סלולרי עלול לעלות כסף במידה וחבילת הגלישה בעלת נפח מוגבל ***"
            verticalAlignment: Text.AlignVCenter
            horizontalAlignment: "AlignHCenter"
            wrapMode: "WrapAtWordBoundaryOrAnywhere"
        }
        tools: commonTools
    }

    ToolBarLayout {
        id: commonTools
        visible: true
        ToolIcon {
            platformIconId: "toolbar-view-menu"
            anchors.right: (parent === undefined) ? undefined : parent.right
            onClicked: (myMenu.status == DialogStatus.Closed) ? myMenu.open() : myMenu.close()
        }

        ToolIcon {
            platformIconId: (audioPlayback.source != "" && audioPlayback.playing)? "toolbar-mediacontrol-stop" : "toolbar-mediacontrol-play"
            anchors.left: (parent === undefined) ? undefined : parent.left
            onClicked: {
                if (audioPlayback.source == "")
                {
                    myMenu.open();
                }
                else
                {
                    if (audioPlayback.playing)
                    {
                        stopClicked = true;
                        audioPlayback.stop();
                    }
                    else
                    {
                        audioPlayback.play();
                    }
                }
            }
        }

        BusyIndicator {
            platformStyle: BusyIndicatorStyle { size: "small" }
            running: audioPlayback.playing && audioPlayback.bufferProgress < 1
            visible: running
            anchors.horizontalCenter: (parent === undefined) ? undefined : parent.horizontalCenter
            anchors.verticalCenter: (parent === undefined) ? undefined : parent.verticalCenter
        }
    }

    Menu {
        id: myMenu
        visualParent: pageStack
        MenuLayout {
            MenuItem { text: "88FM"; onClicked: selectStation(text, "mms://s67wm.castup.net/990310006-52.wmv")}
            MenuItem { text: "גלגל\"צ"; onClicked: selectStation(text, "http://radio.glz.co.il:8000/galgalatz") }
            MenuItem { text: "גל\"צ"; onClicked: selectStation(text, "http://radio.glz.co.il:8000/galatz") }
            MenuItem { text: "רשת א'"; onClicked: selectStation(text, "mms://s67wm.castup.net/990310002-52.wmv") }
            MenuItem { text: "רשת ב'"; onClicked: selectStation(text, "mms://s4awm.castup.net/990310001-52.wmv") }
            MenuItem { text: "רשת ג'"; onClicked: selectStation(text, "mms://s4awm.castup.net/990310004-52.wmv") }
            MenuItem { text: "רשת ד' - عربيل"; onClicked: selectStation(text, "mms://s67wm.castup.net/990310003-52.wmv") }
            MenuItem { text: "Reka"; onClicked: selectStation(text, "mms://s67wm.castup.net/990310007-52.wmv") }
            MenuItem { text: "קול המוסיקה"; onClicked: selectStation(text, "mms://s4awm.castup.net/990310008-52.wmv") }
            //MenuItem { text: "90 FM - רדיו אמצע הדרך"; onClicked: selectStation(text, "mms://192.117.240.26/radio") }
            //MenuItem { text: "99 FM - Esc"; onClicked: selectStation(text, "mms://192.117.240.26/radio") }
            //MenuItem { text: "100 FM - רדיוס"; onClicked: selectStation(text, "http://www.fm1.co.il/playnow/100fm.asx") }
            //MenuItem { text: "102 FM - רדיו תל אביב"; onClicked: selectStation(text, "http://www.fm1.co.il/playnow/102fm.asx") }
            //MenuItem { text: "103 FM"; onClicked: selectStation(text, "mms://live.103.fm/103fm-low") }
            //MenuItem { text: "106 FM - קול הקמפוס"; onClicked: selectStation(text, "http://www.fm1.co.il/playnow/campus.asx") }
            //MenuItem { text: "107.5 FM - רדיו חיפה"; onClicked: selectStation(text, "http://www.fm1.co.il/playnow/radio-haifa.asx") }
        }
    }

    Audio {
        id: audioPlayback

        onStopped: {
            if (!stopClicked)
            {
                audioPlayback.play();
            }
        }

        onPlayingChanged: {
            if (playing) stopClicked = false;
        }

        onError: stationName.text = "שגיאה בהשמעת תחנה, נסו שנית בעוד מספר דקות"
    }
}
